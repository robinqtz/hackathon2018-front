//---------JAVASCRIPT FUNKSJONER---------//

let osmUrl;
let osmAttrib;
let osm;


function getLocation() {
  // TODO : Flytt location kode inn hit.
}

// TODO legge til getMap funksjon


// Henter helsedata
function getHealthServices(lat, lon) {
  let apiUrl = 'http://dasher.alexanderhansen.no:5000/healthservices';
  apiUrl += "?latitude=" + lat;
  apiUrl += "&longitude=" + lon;

  $.ajax({
    url: apiUrl,
    type: 'GET',
    error : function(err) {
      // Håndtering av feil
    },
    success : function(data) {
      let myArray = JSON.parse(data);
      let dataArray = [];
      for (i = 0; i < myArray.length; i++) {
        console.log(myArray[i]);
        let geojsonData = {
          "type" : "Feature",
          "Properties": {
            "name": myArray[i].DisplayName,
            "popupContent": "",
          },
          "geometry": {
            "type": "Point",
            "coordinates": [myArray[i].Latitude, myArray[i].Longitude],
          }
        }; // Avslutter geojsonData

        dataArray.push(geojsonData);

        //var newLayer = L.geoJSON().addTo(map);
        //newLayer.addData(geoObj);
      }
      L.geoJSON(dataArray, {
        filter: function(feature, layer) {
          return feature.properties.show_on_map;
        }
      }).addTo(map);
    },
  });
}
