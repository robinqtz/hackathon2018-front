from flask import Flask
from flask import request
from flask_cors import CORS
import requests

app = Flask(__name__)
CORS(app)
session = requests.Session()

@app.route('/healthservices')
def getHealthservices():
    lat = request.args.get('latitude', '')
    lon = request.args.get('longitude', '')
    api_url = str('http://data.helsenorge.no/healthservices?latitude={}&longitude={}'.format(lat, lon))
    req = session.get(api_url)
    if req.status_code == 200:
        return(req.text)